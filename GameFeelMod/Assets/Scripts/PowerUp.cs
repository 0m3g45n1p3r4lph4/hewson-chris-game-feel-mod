﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//script used by the power up objects
//these'll have different effects to benefit the player
//they all have unique particle effects to differentiate them

public class PowerUp : MonoBehaviour {

    public int effect;
    // 1 = extra ball
    // 2 = paddle speed up
    // 3 = multiball
    // 4 = Score++

    public GameObject gameManagement;
    public GameObject paddle;
    public GameObject multiBall;

    //find our objects
    void Start() {
        gameManagement = GameObject.Find("GameManager");
        paddle = GameObject.Find("Paddle");
        Invoke("SelfDestruct", 15);
        //ensure they don't persist forever
    }

    //run the effects for each power up variant
    void OnCollisionEnter(Collision collision) {
        if (effect == 1) {
            gameManagement.GetComponent<GameManager>().BallCount++;
            gameManagement.GetComponent<GameManager>().ScoreCount += 5;
        } else if (effect == 2) {
            paddle.GetComponent<Paddle>().maxSpeed += 7;
            gameManagement.GetComponent<GameManager>().ScoreCount += 5;
        } else if (effect == 3) {
            Instantiate(multiBall);
            Instantiate(multiBall, new Vector3(-2, 0, 0), Quaternion.identity);
            Instantiate(multiBall, new Vector3(2, 0, 0), Quaternion.identity);
            gameManagement.GetComponent<GameManager>().ScoreCount += 5;
        } else if (effect == 4) {
            gameManagement.GetComponent<GameManager>().ScoreCount+= 15;
        }
        Destroy(this.gameObject);
    }

    void SelfDestruct() {
        Destroy(this.gameObject);
    }
}
