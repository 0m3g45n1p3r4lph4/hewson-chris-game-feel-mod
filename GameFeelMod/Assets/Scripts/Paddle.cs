﻿using UnityEngine;

//script used by the paddle
//it looks like a sick Forcefield thing

public class Paddle : MonoBehaviour {

    //establishing any variables
    public float maxSpeed;
    public float forceMultipler;
    float horizontalInput;
    Rigidbody rigidbodyComponent;
    public GameObject mainCamera;

    void Awake() {
        // get references to components
        rigidbodyComponent = GetComponent<Rigidbody>();
        iTween.Init(this.gameObject);
    }

    void Update() {
        // record input
        horizontalInput = Input.GetAxisRaw("Horizontal");
    }

    void FixedUpdate() {
        // if paddle has not reached top speed
        if (Mathf.Abs(rigidbodyComponent.velocity.x) < maxSpeed) {
            // add force to the paddle if player is pressing left or right
            rigidbodyComponent.AddForce(new Vector3(horizontalInput * forceMultipler, 0, 0));
        }
    }


    //when the paddle collides with stuff.
    //it'll have a nice rubbery effect to it
    //as well as messing with the Chromatic Abberation on the Camera
    void OnCollisionEnter(Collision collision) {
        iTween.StopByName(this.gameObject,"Wobble");
        this.gameObject.transform.localScale = new Vector3(1.5f,0.2f,1);
        iTween.PunchScale(this.gameObject, iTween.Hash("name", "Wobble", "amount", new Vector3(1, 1, 1), "time", 0.5f, "onComplete", "FixBox"));
        iTween.ValueTo(this.gameObject, iTween.Hash("from", 1f, "to", 0.25f, "time", 1f, "easeType", "easeOutQuad", "onStart", "OnChromaUpdate", "onUpdate", "OnChromaUpdate"));
        if (collision.gameObject.tag == "Ball") {
            //Debug.Log("SLAP BALL");
            //please excuse the debug quote, it was not registering the ball for some reason
        }
    }

    //the aforementioned Chromatic Abberation
    void OnChromaUpdate(float newValue) {
        mainCamera.GetComponent<ChromaticAberration>().chromaticAberration = newValue;
    }
}
