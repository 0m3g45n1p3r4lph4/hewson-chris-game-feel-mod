﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//here's where we keep the big sauce
//if something needs to be managed constantly its probably in here unless it isn't 

public class GameManager : MonoBehaviour {

    //attempts at handling FMOD Unity - horizontally adaptive music. really salty about that
    [FMODUnity.EventRef]
    FMOD.Studio.EventInstance BGM;
    FMOD.Studio.ParameterInstance GameState;
    
    public float gameState;

    public Text ScoreText;
    public Text BallText;
    public Text GameOverText;
    public Text WinText;

    public int ScoreCount;
    public int BallCount = 7;
    public int BrickCount = -1;
    bool grandFinale;
    bool endGame;
    bool gameWin;

    public GameObject currentBall;
    public GameObject[] multiBalls;
    public GameObject finalBall;
    public GameObject gameOver;
    public GameObject youWin;
    public GameObject paddle;
    public GameObject bricks;

    // Start is called before the first frame update
    void Start() {
        BGM = FMODUnity.RuntimeManager.CreateInstance("event:/BGM");
        BGM.getParameter("GameState", out GameState);
        BGM.start();
    }

    // Update is called once per frame
    void Update() {
        //make sure our data is transmitting to where it needs to be used properly
        ScoreText.text = ScoreCount.ToString();
        BallText.text = BallCount.ToString();
        GameOverText.enabled = endGame;
        WinText.enabled = gameWin;
        GameState.setValue(gameState);
        multiBalls = GameObject.FindGameObjectsWithTag("Ball");
        
        //final ball, game over
        if (BallCount == 0 && !grandFinale) {
            Instantiate(finalBall);
            grandFinale = true;
        } if (BallCount < 0 && !endGame) {
            Instantiate(gameOver);
            endGame = true;
            Destroy(currentBall);
            Destroy(paddle);
            Destroy(bricks);
        }

        //winning the game
        if (BrickCount <= 1 && !gameWin) {
            gameWin = true;
            Time.timeScale = 0.5f;
            Invoke("WinState", 2f);
        }

        //either ending, get rid of extra objects
        if (gameWin || endGame) {
            Destroy(multiBalls[Random.Range(0,multiBalls.Length)]);
        }
    }

    //what happens when you win
    //set on a slight delay for dramatic effect
    void WinState() {
        Time.timeScale = 1;
        WinText.enabled = true;
        Instantiate(youWin);
        Destroy(currentBall);
        Destroy(paddle);
    }
}
