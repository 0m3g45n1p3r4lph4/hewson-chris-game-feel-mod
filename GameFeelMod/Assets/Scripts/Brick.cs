﻿using UnityEngine;

//script used by the individual bricks

public class Brick : MonoBehaviour {

    //establish any variables
    BoxCollider brickBox;
    ParticleSystem particles;
    public GameObject[] powerUps;
    public GameObject gameManagement;

    //getting any components & starting up any functions
    void Awake() {
        brickBox = GetComponent<BoxCollider>();
        particles = GetComponent<ParticleSystem>();
        //getting the game manager
        gameManagement = GameObject.Find("GameManager");
        gameManagement.GetComponent<GameManager>().BrickCount++;
        iTween.Init(this.gameObject);
    }

    void OnCollisionExit (Collision collision) {
        //so here's what it does
        /* we disable the box collider immediately
         * it does a PunchScale so that it has a cool bounce effect
         * we have it fade to a transparent white
         * it spins around and thins out, rising higher as it dissapears
         * 
         * change of plans its not rotating
         */ 
        brickBox.enabled = false;
        iTween.PunchScale(this.gameObject, iTween.Hash("amount", new Vector3(1,1,1), "time", 3f));
        iTween.ColorTo(this.gameObject, iTween.Hash("a", 0, "r", 1, "g", 1, "b", 1, "time", 2f, "onComplete", "Completed"));
        //iTween.RotateBy(this.gameObject, iTween.Hash("amount", new Vector3(0,3,0), "time", 2f, "delay", 0.25f));
        iTween.ScaleTo(this.gameObject, iTween.Hash("scale", new Vector3(0,1,0), "time", 2f, "delay", 0.75f));
        iTween.MoveBy(this.gameObject, iTween.Hash("y", 2, "time", 5f, "delay", 0.75f));
        Instantiate(powerUps[Random.Range(1,powerUps.Length)], new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y), Quaternion.identity);
	}

    //when all is said and done
    void Completed() {
        var emitParams = new ParticleSystem.EmitParams();
        particles.Emit(emitParams, 25);
        gameManagement.GetComponent<GameManager>().BrickCount--;
        //Destroy(this.gameObject);
    }

}
