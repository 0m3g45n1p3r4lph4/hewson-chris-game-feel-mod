﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//simple script used by power up blanks to reduce storage being used

public class SelfDestruct : MonoBehaviour {
    // Start is called before the first frame update
    void Start() {
        Destroy(this.gameObject);
    }
}
