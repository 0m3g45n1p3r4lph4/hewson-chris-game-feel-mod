﻿using UnityEngine;

public class Ball : MonoBehaviour {

    //that is... a LOT of variables.
    public float speed;
    public bool hasStarted;
    Vector3 initialPosition;
    public float maxHitAngle;
    public AudioClip[] bounceSounds;
    public GameObject ballPrefab;
    Rigidbody rigidbodyComponent;
    int dropTimer;
    FMOD.Studio.EventInstance fireBall;
    public bool multiBall;
    public GameObject multiBallPrefab;
    public GameObject paddle;
    public GameObject gameManagement;
    public int ScoreCount;
    public int BallCount;
    public float gameState;


    void Awake() {
        // get references to components
        rigidbodyComponent = GetComponent<Rigidbody>();
        initialPosition = transform.position;
        fireBall = FMODUnity.RuntimeManager.CreateInstance("event:/fireBall");
        gameManagement = GameObject.Find("GameManager");
        paddle = GameObject.Find("Paddle");
        gameManagement.GetComponent<GameManager>().currentBall = this.gameObject;
        iTween.Init(this.gameObject);
    }

    void Start() {
        //actually, let's have it not mve until the player presses up or something
        hasStarted = false;
        rigidbodyComponent.velocity = new Vector3(0,0);
    }

    void FixedUpdate() {
        // fix the ball's velocity so that it is constant
        rigidbodyComponent.velocity = rigidbodyComponent.velocity.normalized * speed;
    }

    void Update() {
        //cap the speed so it doesn't break
        if (speed >= 17) {
            speed = 17;
        }

        //start the ball's movement on input
        if (!hasStarted && Input.GetKeyDown(KeyCode.UpArrow)) {
            rigidbodyComponent.velocity = Random.insideUnitCircle;
            hasStarted = true;
            gameState = 0.5f;
        }
        //fireball dash move - ready
        if (dropTimer == 7 && !multiBall) {
            iTween.ColorTo(this.gameObject, Color.white, 0);
            iTween.ColorFrom(this.gameObject, iTween.Hash("name", "ReadyReddy", "color", Color.red, "time", 0.5f));
            dropTimer--;
        } else if (dropTimer >= 0 && !multiBall) {
            dropTimer--;
        } else if (dropTimer < 0 && !multiBall) {
            //fireball dash move - activated
            if (Input.GetKeyDown(KeyCode.DownArrow)) {
                rigidbodyComponent.velocity -= new Vector3(0, 10);
                fireBall.start();
                iTween.ColorFrom(this.gameObject, Color.red, 1);
                dropTimer = 250;
                fireBall.release();
            }
        }

        //if (Input.GetKeyDown(KeyCode.Space) && !multiBall) {
            //Instantiate(multiBallPrefab, new Vector3(0,-.26f,0), Quaternion.identity);
            //Debug.Log("M U L T I B A L L        Have a fantastic time!");
        //}
    }

    //respawning the ball
    void Reset() {
        Instantiate(ballPrefab, initialPosition, this.gameObject.transform.rotation);
        speed-= 5;
        gameManagement.GetComponent<GameManager>().BallCount--;
        if (speed < 6) {
            speed = 6;
        }
        iTween.ColorTo(this.gameObject, Color.white, 0);
        Destroy(this.gameObject);
    }

    //colliding with the dead zone
    void OnTriggerEnter(Collider other) {
        if (other.tag == "Dead Zone" && !multiBall && gameManagement.GetComponent<GameManager>().BallCount >= 0) {
            Invoke("Reset", 2f);
            gameState = 0;
        } else if (other.tag == "Dead Zone") {
            Destroy(this.gameObject);
        }
    }


    //colliding with anything else
    void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Brick") {
            //rigidbodyComponent.velocity += new Vector3(rigidbodyComponent.velocity.x,rigidbodyComponent.velocity.y);
            speed += 0.25f;
            if (speed >= 17) {
                speed = 17;
            }
            ScoreCount++;
            gameManagement.GetComponent<GameManager>().ScoreCount++;
            Debug.Log(speed);
        } if (collision.gameObject.tag == "Wall") {
            speed -= 0.10f;
        } if (collision.gameObject.tag == "Paddle") {
            this.gameObject.layer = 9;
            Invoke("FixBall", 1);
            paddle.transform.localScale = new Vector3(1.5f, 0.2f, 1);
            iTween.PunchScale(paddle, iTween.Hash("name", "Wobblie", "amount", new Vector3(1, 1, 1), "time", 0.5f, "onComplete", "FixBox"));
        }
    }

    //making sure the layering doesn't make the ball phaze through the paddle
    void FixBall() {
        this.gameObject.layer = 10;
    }
}
