Advanced Breakout

Move the paddle with the arrow keys (left and right)
activate the ball with the up arrow, it'll launch in a random direction
when available to use your fireball ability, the ball will flash red. after this, pressing the down arrow will boost the ball downards at high speeds - good for getting unstuck

there are 4 powerups that can drop from bricks
-Powerup - significantly increses score, indicated with yellow
-Multiball - summons 3 free balls that don't count towards your limit. activate them with Up. multiballs do not have a fireball move, and have a different glow.
-Speed Up - incrreases the movement speed of the paddle.
-Extra Ball - gives you one more ball in your total.

you win if you break all the bricks, and lose if you run out of balls from them falling below the paddle.